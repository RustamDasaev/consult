captchaSecret = null;

//---- Метод взят с https://gist.github.com/larchanka/9367841---
String.prototype.hashCode = function () {
    var hash = 0;

    try {

        if (this.length == 0) return hash;

        for (i = 0; i < this.length; i++) {
            char = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash; // Convert to 32bit integer
        }
        return hash;

    } catch (e) {
        throw new Error('hashCode: ' + e);
    }
};
//--------------------------------------------------------------

function getUnique() {
    var size = 5;
    var result = "";
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < size; i++)
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    return result;
}

function reCaptcha() {
    var captcha = getUnique();
    var ctx = null;
    var x = 0;

    ctx = $('canvas')[0].getContext('2d');
    ctx.fillStyle = 'gray';
    ctx.fillRect(0, 0, 300, 150);

    ctx.fillStyle = 'white';
    ctx.font = '40px Arial';

    for (var i = 0; i < captcha.length; i++) {
        x += 10 + Math.random() * 50;
        ctx.fillText(captcha.charAt(i), x, 60 + Math.random() * 40);
    }

    captchaSecret = captcha.hashCode();
}

function errorInput(input) {
    input.css('border', '1px solid red');
    input.focus();
    return false;
}

// Простейшая валидация емаила, просто для примера
function checkEmail(email) {
    email.css('border', '');
    if (/^[0-9a-z]+@[0-9a-z]+\.[a-z]{1,3}$/i.test(email.val()) == false) {
        return errorInput(email);
    }
    return true;
}

function notEmpty(input) {
    input.css('border', '');
    if (input.val() == '') {
        return errorInput(input);
    }
    return true;
}

function checkCaptcha() {
    var input = $('input[name=captcha]');
    input.css('border', '');
    if (input.val().hashCode() != captchaSecret) {
        return errorInput(input);
    }
    return true;
}

$(function () {
    var email = $('input[name=email]');

    email.change(function () {
        checkEmail($(this));
    });

    $('#reCaptcha').click(function () {
        reCaptcha();
    });

    $('button[name=openForm]').click(function () {
        reCaptcha();
        $('#background').css('display', 'block').animate({opacity: 0.8}, 500);
        $('#textContainer').css('display', 'block').animate({opacity: 1, top: '10%'}, 1000);
    });

    $('button[name=send]').click(function () {
        if (notEmpty($('input[name=face]')) && checkEmail(email) && notEmpty($('textarea')) && checkCaptcha()) {
            $('#sendText').toggle().html($('textarea').val());
            $('#form').toggle();
            setTimeout(function () {
                $('#form :input').val('');
                $('#sendText').toggle().html('');
                $('#form').toggle();
                reCaptcha();
            }, 2500);
        }
    });
});